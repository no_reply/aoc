#!/usr/bin/ruby

lines = File.readlines(ARGV[0], chomp: true)

index = 0
oxygen = lines.dup

while oxygen.size > 1 do
  bits = oxygen.map { |b| b[index] }
  common = bits.sort[(bits.size / 2.0).round(half: :down)]
  oxygen.select! { |line| line[index] == common }
  index += 1
end

puts oxygen = oxygen.first.to_i(2)

index = 0
scrubber = lines

while scrubber.size > 1 do
  bits = scrubber.map { |b| b[index] }
  common = bits.sort[(bits.size / 2.0).round(half: :down)]

  scrubber.reject! { |line| line[index] == common.to_s }
  index += 1
end

puts scrubber = scrubber.first.to_i(2)

puts oxygen * scrubber
