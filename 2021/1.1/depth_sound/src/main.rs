use std::env;
use std::fs::File;
use std::io::{self, BufRead};

fn main() -> std::io::Result<()>  {
    let args: Vec<String> = env::args().collect();
    let file = File::open(&args[1])?;
    let mut last_depth = u16::MAX;

    let count = io::BufReader::new(file).lines().filter(|line| {
        let depth: u16 = line.as_ref().unwrap().parse().unwrap();
        let result = depth > last_depth;
        last_depth = depth;
        result
    }).count();

    println!("Depth increases:\n{}", count);

    Ok(())
}
