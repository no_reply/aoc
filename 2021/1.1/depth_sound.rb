#!/usr/bin/ruby

last_depth = Float::INFINITY

puts(File.open(ARGV[0]).select do |line|
  depth = line.to_i
  deeper = depth > last_depth
  last_depth = depth
  deeper
end.count)
