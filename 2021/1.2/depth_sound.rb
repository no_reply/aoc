#!/usr/bin/ruby

Sweep = Struct.new(:depths) do
  include Comparable

  ##
  # @return [Sweep]
  def sweep(i)
    Sweep.new(depths.last(2) << i)
  end

  ##
  # @return [Integer]
  def <=>(other)
    depths.sum <=> other.depths.sum
  end
end

depth = last_depth = Sweep.new([Float::INFINITY, Float::INFINITY, Float::INFINITY])

puts (File.open(ARGV[0]).select do |line|
  depth = depth.sweep(line.to_i)
  deeper = depth > last_depth
  last_depth = depth
  deeper
end.count)
