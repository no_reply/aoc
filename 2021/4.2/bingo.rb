#!/usr/bin/ruby

class Bingo
  def initialize(input)
    @boards = Board.boards_for(input)
  end

  def draw_until_winner(ns)
    ns.find { |n| draw(n.to_i) }
    @boards.find(&:won?)
  end

  def find_last_winner(ns)
    until @boards.count == 1 do
      draw(ns.shift.to_i)
      @boards -= @boards.select(&:won?)
    end

    draw_until_winner(ns)
  end

  def draw(n)
    @boards.each { |board| board.mark(n) }
    winner?
  end

  def winner?
    @boards.any?(&:won?)
  end
end

class Board
  def initialize(rows:)
    rows.reject!(&:empty?)
    @rows = rows.map { |row| row.split(/\s+/).reject(&:empty?).map(&:to_i) }
    @columns = @rows.transpose

    @marked = []
  end

  def self.boards_for(input)
    boards = []

    while input.any? do
      boards << Board.new(rows: input.shift(6))
    end

    boards
  end

  def mark(n)
    @marked << n
  end

  def score
    (@rows.flatten - @marked).sum * @marked.last
  end

  def won?
    @rows.find { |row| (row - @marked).empty? } ||
      @columns.find { |col| (col - @marked).empty? }
  end
end

file = File.open(ARGV[0])

drawings = file.readline(chomp: true).split(',')
game = Bingo.new(file.readlines(chomp: true))

puts game.find_last_winner(drawings).score
