#!/usr/bin/ruby

lines = File.readlines(ARGV[0], chomp: true).map(&:chars)

digits = lines.transpose.map do |digit|
  digit.sort[(digit.size / 2.0).round(half: :down)]
end.join

gamma = digits.to_i(2)
epsilon = digits.gsub('0', 'n').gsub('1', '0').gsub('n', '1').to_i(2)

puts gamma * epsilon
