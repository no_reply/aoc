#!/usr/bin/ruby

class Position
  attr_reader :x, :y

  def initialize
    @x = 0
    @y = 0
  end

  def down(n)
    @y += n.to_i
  end

  def forward(n)
    @x += n.to_i
  end

  def up(n)
    @y -= n.to_i
  end
end

position = Position.new

File.open(ARGV[0]).each do |line|
  position.send(*line.split(' '))
end

puts position.x * position.y
