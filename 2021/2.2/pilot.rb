#!/usr/bin/ruby

class Position
  attr_reader :x, :y

  def initialize
    @aim = 0
    @x = 0
    @y = 0
  end

  def down(n)
    @aim += n.to_i
  end

  def forward(n)
    @x += n.to_i
    @y += (@aim * n.to_i)
  end

  def up(n)
    @aim -= n.to_i
  end
end

position = Position.new

File.open(ARGV[0]).each do |line|
  position.send(*line.split(' '))
end

puts position.x * position.y
