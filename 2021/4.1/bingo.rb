#!/usr/bin/ruby

class Bingo
  def initialize(input)
    @drawn = []
    @boards = Board.boards_for(input.reject!(&:empty?), marked: @drawn)
  end

  def draw_until_winner(ns)
    ns.find { |n| draw(n.to_i) }
    @boards.find(&:won?)
  end

  def draw(n)
    @drawn << n
    winner?
  end

  def winner?
    @boards.any?(&:won?)
  end
end

class Board
  def initialize(rows:, marked: [])
    @rows = rows.map { |row| row.strip.split(/\s+/).map(&:to_i) }
    @columns = @rows.transpose

    @marked = marked
  end

  def self.boards_for(input, marked: [])
    boards = []

    while input.any? do
      boards << Board.new(rows: input.shift(5), marked: marked)
    end

    boards
  end

  def score
    (@rows.flatten - @marked).sum * @marked.last
  end

  def won?
    @rows.find { |row| (row - @marked).empty? } ||
      @columns.find { |col| (col - @marked).empty? }
  end
end

file = File.open(ARGV[0])

drawings = file.readline(chomp: true).split(',')
game = Bingo.new(file.readlines(chomp: true))

puts game.draw_until_winner(drawings).score
